package k

import j.Target

fun main(args: Array<String>) {
    //val s : String = null  <-- Will not compile
    val s : String? = null

    case01()
    case02()
    case03()
    case04()
    case05()
    case06()
    case07()
    case08()
}

fun case01() {
    val sNotNullable : String
    val t = Target()

    // The Java method might return a null value but by inspection we
    // know that this is not possible. We assign the returned value to
    // a non-nullable String type.
    sNotNullable = t.javaTargetMethod("Hello!")
}

fun case02() {
    val t = Target()

    // ???
    val sNotNullable: String? = t.javaTargetMethod(null)  // Exception thrown from here
}

fun case03() {
    val t : Target = Target()

    // The Java method might return a null value. By using the
    // nullable version of the String type the programmer signals
    // to the Kotlin compiler that they understand this and will
    //write appropriate code.
    val s : String? = t.javaTargetMethod("hello")
}

fun case04() {
    val t : Target = Target()
    val s : String? = t.javaTargetMethod("hello")

    // The compiler know that the variable 's' could have a null
    // value and does not permit a successful compilation
    //println(s.length)  <-- Will not compile
}

fun case05() {
    val t : Target = Target()
    val s : String? = t.javaTargetMethod("hello")

    // The variable 's' could be set to null.
    // The compiler can determine after what point it is not
    // possible to have a null value in 's' and de-referencing is fully safe
    if (s != null && s.length > 0)
        println(s.length)
    else
        println("Empty String")
}


fun case06() {
    val t : Target = Target()
    val s : String? = t.javaTargetMethod("hello")

    // Use an object reference or propagate a null value
    // The value of 'l' will either be the length of a String referenced
    // by the 's' value or it will be null.
    val l : Int? = s?.length
}

fun case07() {
    val t : Target = Target()
    val s : String? = t.javaTargetMethod("hello")

    // Stop the propagation of a potentially null value in the object reference 's'.
    // The variable 'l' with either be the length of the String referenced by 's'
    // or it will be the value -1.
    val l : Int = s?.length ?: -1
}

fun case08() {
    val t : Target = Target()
    val s : String? = t.javaTargetMethod("hello")

    // Use the object reference s as if this is Java code. This will work
    // if the reference is not null and will throw an NPE if it is null
    // whether or not it has a null value
    val l = s!!.length
}